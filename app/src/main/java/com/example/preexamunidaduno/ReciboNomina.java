package com.example.preexamunidaduno;

import android.content.Context;
import android.widget.Toast;

import java.io.Serializable;
import java.util.Random;

public class ReciboNomina implements Serializable {
    private int numRecibo;
    private String Nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float impuestoPorc;

    public ReciboNomina(String Nombre, float horasTrabNormal, float horasTrabExtras, int puesto, float impuestoPorc) {
        this.numRecibo = generarRecibo();
        this.Nombre = Nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }

    public ReciboNomina() {
        this.numRecibo = generarRecibo();
        this.Nombre = "";
        this.horasTrabNormal = 0.0f;
        this.horasTrabExtras = 0.0f;
        this.puesto = 0;
        this.impuestoPorc = 0.0f;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    public float calcularSubtotal(Context context){
        if (horasTrabNormal < 0 || horasTrabExtras < 0){
            Toast.makeText(context, "Las horas no pueden ser negativas", Toast.LENGTH_SHORT).show();
        }
        int pagoBase = 0;
        if (puesto == 1) {
            pagoBase = 200;
        } else if (puesto == 2) {
            pagoBase = 200;
        } else if (puesto == 3) {
            pagoBase = 200;
        }

        float pagoPorHora = pagoBase;

        if (puesto == 1) {
            pagoPorHora += (pagoBase * 0.2f); // 20% de incremento
        } else if (puesto == 2) {
            pagoPorHora += (pagoBase * 0.5f); // 50% de incremento
        } else if (puesto == 3) {
            pagoPorHora += (pagoBase * 1.0f); // 100% de incremento
        }

        return horasTrabNormal * pagoPorHora + horasTrabExtras * pagoPorHora * 2;
    }

    public float calcularImpuesto(Context context){
        float subtotal = calcularSubtotal(context);
        return subtotal * 0.16f;
    }

    public float calcularTotal(Context context){
        float subtotal = calcularSubtotal(context);
        float impuesto = calcularImpuesto(context);
        return subtotal - impuesto;
    }

    public int generarRecibo(){
        Random r = new Random();
        return r.nextInt(1000);
    }
}
