package com.example.preexamunidaduno;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class ReciboNominaActivity extends AppCompatActivity {
    private TextView lblNombre, lblNumRecibo, lblSubtotal, lblImpuesto, lblTotal;
    private EditText txtHorasNormal, txtHorasExtras;
    private Button btnCalcular, btnLimpiar, btnRegresar;
    private RadioGroup groupPuestos;
    private RadioButton rdbAuxiliar, rdbAlbanil, rdbIngObra;
    private ReciboNomina reciboNomina;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_recibo_nomina);
        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        lblNombre = findViewById(R.id.txtNombre);
        lblNumRecibo = findViewById(R.id.txtNumRecibo);
        lblSubtotal = findViewById(R.id.txtSubtotal);
        lblImpuesto = findViewById(R.id.txtImpuestop);
        lblTotal = findViewById(R.id.txtTotal);
        txtHorasNormal = findViewById(R.id.txtHorasNormal);
        txtHorasExtras = findViewById(R.id.txtHorasExtras);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        groupPuestos = findViewById(R.id.groupPuestos);
        rdbAuxiliar = findViewById(R.id.rdbAuxiliar);
        rdbAlbanil = findViewById(R.id.rdbAlbanil);
        rdbIngObra = findViewById(R.id.rdbIngObra);

        recibirDatos();
        reciboNomina = new ReciboNomina();

        lblNumRecibo.setText(String.valueOf(reciboNomina.getNumRecibo()));
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float horasNormal;
                float horasExtras;
                try {
                    horasNormal = Float.parseFloat(txtHorasNormal.getText().toString());
                    horasExtras = Float.parseFloat(txtHorasExtras.getText().toString());
                } catch(NumberFormatException e) {
                    Toast.makeText(ReciboNominaActivity.this, "Introduce horas validas", Toast.LENGTH_SHORT).show();
                    return;
                }

                int puestoId = groupPuestos.getCheckedRadioButtonId();
                if (puestoId == -1) {
                    Toast.makeText(ReciboNominaActivity.this, "Selecciona un puesto", Toast.LENGTH_SHORT).show();
                    return;
                }

                int puesto = 0;
                if (puestoId == R.id.rdbAuxiliar){
                    puesto = 1;
                } else if (puestoId == R.id.rdbAlbanil) {
                    puesto = 2;
                } else if (puestoId == R.id.rdbIngObra) {
                    puesto = 3;
                }

                reciboNomina.setHorasTrabNormal(horasNormal);
                reciboNomina.setHorasTrabExtras(horasExtras);
                reciboNomina.setPuesto(puesto);

                Context context = ReciboNominaActivity.this;

                float subtotal = reciboNomina.calcularSubtotal(context);
                float impuesto = reciboNomina.calcularImpuesto(context);
                float total = reciboNomina.calcularTotal(context);

                lblSubtotal.setText(String.format("%.2f", subtotal));
                lblImpuesto.setText(String.format("%.2f", impuesto));
                lblTotal.setText(String.format("%.2f", total));
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lblSubtotal.setText("");
                lblImpuesto.setText("");
                lblTotal.setText("");
                txtHorasNormal.setText("");
                txtHorasExtras.setText("");
                groupPuestos.clearCheck();
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    private void recibirDatos(){
        Bundle extras = getIntent().getExtras();
        if (extras != null){
            reciboNomina = (ReciboNomina) extras.getSerializable("reciboNomina");
            // No necesitas verificar si reciboNomina es null aquí, ya que siempre debería estar presente si "reciboNomina" está en los extras
            lblNombre.setText(reciboNomina.getNombre());
        }
    }
}